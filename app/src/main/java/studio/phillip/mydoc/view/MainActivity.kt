package studio.phillip.mydoc.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import studio.phillip.mydoc.R
import studio.phillip.mydoc.data.local.entity.ArticleEntity
import studio.phillip.mydoc.data.remote.Status
import studio.phillip.mydoc.data.remote.repository.ArticleRepository
import studio.phillip.mydoc.view.adapter.ArticleListAdapter
import studio.phillip.mydoc.viewmodel.ArticleListViewModel
import androidx.recyclerview.widget.LinearLayoutManager as LinearLayoutManager1

class MainActivity : AppCompatActivity(), ArticleListAdapter.ArticleListener {
    private lateinit var rvArticle: RecyclerView

    lateinit var articleRepository: ArticleRepository
    private lateinit var viewModel: ArticleListViewModel
    private lateinit var adatper: ArticleListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initArticleList()
        getViewModel()

        viewModel.popularArticles.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
//                    dataBinding.progress.visibility = View.VISIBLE
//                    viewModel.loading = true
                }
                Status.ERROR -> {
//                    dataBinding.progress.visibility = View.GONE
//                    viewModel.loading = true
//                    activity?.let { ac ->
//                        Toast.makeText(ac, it.message, Toast.LENGTH_SHORT).show()
//                    }
                }
                Status.SUCCESS -> {
//                    dataBinding.progress.visibility = View.GONE
//                    viewModel.loading = false
//                    viewModel.canLoad = it.data != null && it.data.size >= 50
                }


            }
            var list = ArrayList<ArticleEntity>()
            it.data?.forEach { item ->
                list.add(item)
            }
            adatper.addAll(list)
        })
    }

    private fun getViewModel(): ArticleListViewModel {
        articleRepository = ArticleRepository()
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return ArticleListViewModel(articleRepository) as T
            }
        })[ArticleListViewModel::class.java]
    }

    private fun initArticleList() {
        rvArticle = findViewById(R.id.rvArticle)
        adatper = ArticleListAdapter(arrayListOf(), this, this)


        rvArticle.layoutManager = LinearLayoutManager1(this)
        rvArticle.adapter = adatper
//        rvArticle.
//        dataBinding.rvCards.checkReachedLastItem {
//            // if we have reach the end to the recyclerView
//            if (this && viewModel.canLoad && !viewModel.loading) {
//                getData(++viewModel.page)
//            }
//        }
    }

    override fun onArticleClicked(articleEntity: ArticleEntity) {
    }
}

package studio.phillip.mydoc.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import studio.phillip.mydoc.R
import studio.phillip.mydoc.data.local.entity.ArticleEntity

class ArticleListAdapter constructor(
    val items: ArrayList<ArticleEntity>,
    val context: Context,
    val mListener: ArticleListener
) :
    RecyclerView.Adapter<ArticleListAdapter.JobViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_article, parent, false)
        return JobViewHolder(view)
    }

    override fun onBindViewHolder(holder: JobViewHolder, position: Int) {
        val data = items[position]
//        data.agency_name?.apply {
//            holder.txtAgencyName.text = data.agency_name
//        }
//
//        data.store_name?.apply {
//            holder.txtStore.text = data.store_name
//        }
//
//        data.project_name?.apply {
//            holder.txtProjectName.text = data.project_name
//        }
//
//        val start_date = TimeFormatUtils.formatHour(data.shift_start!!.toDate())
//        val end_date = TimeFormatUtils.formatHour(data.shift_end!!.toDate())
//        val shift = start_date.toString() + "-" + end_date
//        holder.txtTime.text = shift.toString()
//
//        holder.itemView.setOnClickListener {
//            mListener.onJobItemClicked(data.id.toString())
//        }
//
//        holder.txtAgencyName.setOnClickListener {
//            mListener.onAgencyItemClicked(data.agency_id.toString(), data.agency_name.toString())
//        }
//
//        holder.txtProjectName.setOnClickListener {
//            mListener.onProjectItemClicked(data.project_id.toString(), data.project_name.toString())
//        }
//
//        holder.txtStore.setOnClickListener {
//            mListener.onStoreItemClicked(data.store_id.toString(), data.store_name.toString())
//        }
    }

    inner class JobViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        @BindView(R.id.txt_agency_name)
//        lateinit var txtAgencyName: TextView
//        @BindView(R.id.txt_store_name)
//        lateinit var txtStore: TextView
//        @BindView(R.id.txt_project_name)
//        lateinit var txtProjectName: TextView
//        @BindView(R.id.txt_shift)
//        lateinit var txtTime: TextView
//
//        init {
//            ButterKnife.bind(this, itemView)
//        }

    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    interface ArticleListener {
        fun onArticleClicked(articleEntity: ArticleEntity)
    }

    fun addAll(jobs: ArrayList<ArticleEntity>) {
        items.addAll(jobs)
        notifyItemInserted(items.size - 1)
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }
}
package studio.phillip.mydoc.view.callback

import studio.phillip.mydoc.data.local.entity.ArticleEntity

interface ResponseListener {
    abstract fun onSuccess(data: ArticleEntity)
    abstract fun onFailure(message: String)
}
package studio.phillip.mydoc.view.callback

import studio.phillip.mydoc.data.local.entity.ArticleEntity

interface ArticleListCallback {
    abstract fun onArticleClicked(articleEntity: ArticleEntity)
}
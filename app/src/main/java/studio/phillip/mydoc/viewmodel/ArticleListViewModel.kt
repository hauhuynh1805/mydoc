package studio.phillip.mydoc.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import studio.phillip.mydoc.data.local.entity.ArticleEntity
import studio.phillip.mydoc.data.remote.Resource
import studio.phillip.mydoc.data.remote.repository.ArticleRepository

class ArticleListViewModel
constructor(articleRepository: ArticleRepository) : ViewModel() {
    val popularArticles: LiveData<Resource<List<ArticleEntity>>>

    init {
        popularArticles = articleRepository.loadPopularArticles(7)
    }
}

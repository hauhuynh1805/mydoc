package studio.phillip.mydoc.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import studio.phillip.mydoc.data.local.entity.ArticleEntity
import studio.phillip.mydoc.data.remote.repository.ArticleRepository
import studio.phillip.mydoc.utils.SingleLiveEvent
import studio.phillip.mydoc.view.callback.ResponseListener

class ArticleDetailsViewModel constructor(private val articleRepository: ArticleRepository?) :
    ViewModel() {

    var url: String? = null

    var articleEntityMutableLiveData = MutableLiveData<ArticleEntity>()

    var errorMessageRecieved = SingleLiveEvent<Void>()

    fun loadArticleDetails() {
        articleRepository?.loadArticleDetails(url!!, object : ResponseListener {
            override fun onSuccess(data: ArticleEntity) {
                articleEntityMutableLiveData.value = data
            }

            override fun onFailure(message: String) {
                errorMessageRecieved.call()
            }
        })
    }
}

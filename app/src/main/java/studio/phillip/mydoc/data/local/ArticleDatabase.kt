package studio.phillip.mydoc.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import studio.phillip.mydoc.data.local.dao.ArticleDao
import studio.phillip.mydoc.data.local.entity.ArticleEntity


@Database(entities = [ArticleEntity::class], version = 1)
abstract class ArticleDatabase : RoomDatabase() {
    abstract fun articleDao(): ArticleDao
}
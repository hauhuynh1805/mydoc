package studio.phillip.mydoc.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "article_table")
class ArticleEntity {
    @PrimaryKey
    @SerializedName("id")
    var id: Long = 0

    @SerializedName("title")
    var title: String? = null

    @SerializedName("byline")
    var authors: String? = null

    @SerializedName("published_date")
    var publishedDate: String? = null

    @SerializedName("url")
    var url: String? = null

    var content: String? = null

}
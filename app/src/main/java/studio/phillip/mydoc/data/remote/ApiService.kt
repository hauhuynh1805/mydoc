package studio.phillip.mydoc.data.remote

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import studio.phillip.mydoc.data.remote.model.ArticleResponse

interface ApiService {

    @GET("svc/mostpopular/v2/emailed/{period}.json")
    abstract fun loadPopularArticles(@Path("period") index: Int): Call<ArticleResponse>
}
package studio.phillip.mydoc.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import studio.phillip.mydoc.data.local.entity.ArticleEntity

@Dao
interface ArticleDao {
    @Query("SELECT * FROM article_table")
    fun loadPopularArticles(): LiveData<List<ArticleEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveArticles(articleEntities: List<ArticleEntity>)

}
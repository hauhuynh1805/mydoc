package studio.phillip.mydoc.data.remote.repository

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jsoup.Jsoup
import retrofit2.Call
import studio.phillip.mydoc.data.local.dao.ArticleDao
import studio.phillip.mydoc.data.local.entity.ArticleEntity
import studio.phillip.mydoc.data.remote.ApiService
import studio.phillip.mydoc.data.remote.NetworkBoundResource
import studio.phillip.mydoc.data.remote.Resource
import studio.phillip.mydoc.data.remote.model.ArticleResponse
import studio.phillip.mydoc.view.callback.ResponseListener

class ArticleRepository constructor(
    private val articleDao: ArticleDao,
    private val apiService: ApiService
) {

    fun loadPopularArticles(howfarback: Int): LiveData<Resource<List<ArticleEntity>>> {
        return object : NetworkBoundResource<List<ArticleEntity>, ArticleResponse>() {

            override fun saveCallResult(item: ArticleResponse?) {
                if (null != item)
                    articleDao.saveArticles(item.popularArticles!!)
            }

            override fun loadFromDb(): LiveData<List<ArticleEntity>> {
                return articleDao.loadPopularArticles()
            }

            override fun createCall(): Call<ArticleResponse> {
                return apiService.loadPopularArticles(howfarback)
            }
        }.asLiveData
    }


    @SuppressLint("CheckResult")
    fun loadArticleDetails(url: String, responseListener: ResponseListener) {
        val articleDetails = ArticleEntity()
        Observable.fromCallable {
            val document = Jsoup.connect(url).get()
            articleDetails.title = document.title()
            articleDetails.content = document.select("p").text()
            false
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result -> responseListener.onSuccess(articleDetails) },
                { error -> error.message?.let { responseListener.onFailure(it) } })
    }

}

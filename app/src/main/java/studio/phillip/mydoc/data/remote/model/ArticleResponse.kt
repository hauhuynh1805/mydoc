package studio.phillip.mydoc.data.remote.model

import com.google.gson.annotations.SerializedName
import studio.phillip.mydoc.data.local.entity.ArticleEntity

class ArticleResponse {

    @SerializedName("results")
    var popularArticles: List<ArticleEntity>? = null
}
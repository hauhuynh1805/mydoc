package studio.phillip.mydoc.data.remote

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
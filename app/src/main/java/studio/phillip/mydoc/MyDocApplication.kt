package studio.phillip.mydoc

import android.app.Application

class MyDocApplication : Application() {
    companion object {
        var appContext: MyDocApplication? = null
            private set

        @Synchronized private fun setInstance(app: MyDocApplication) {
            appContext = app
        }
    }

    override fun onCreate() {
        super.onCreate()
        setInstance(this)
    }

}